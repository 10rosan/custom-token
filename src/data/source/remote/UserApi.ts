import { Auth, UserCredentials } from './../../../types/models.d';
import axios from 'axios';
import { customTokenBaseUrl } from '@/data/source/remote/api-constant';
import { UserDataMapper } from '@/data/mapper/UserDataMapper';


export class UserApi {
  public async loginUser(user: UserCredentials): Promise<Auth> {
    const response = await axios({
      method: 'post',
      url: customTokenBaseUrl + 'auth/login/',
      data: {
        email_or_phone_number: user.email,
        password: user.password,
      },
    })
      .catch((e) => {
        console.log('Login Error');
        console.log(e.response);
        console.log(e.response.data);
        console.log('Login Error');
        throw e;
        //return UserDataMapper.mapToInvalidAuth(e.response.data);
        

      });
    console.log('Response Data');
    console.log(response.data);
    console.log('Response Data');
    console.log('Response Data Result');
    console.log(response.data.result);
    console.log('Response Data Result');
    return UserDataMapper.mapToAuth(response.data);
  }
}
