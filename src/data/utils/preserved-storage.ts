export function getToken(): string | null {
    const data: string | null = localStorage.getItem('accessKeyCustomToken');
    if (data) {
        return data;
    } else {
        return null;
    }
}

export function setToken(token: string) {
    localStorage.setItem('accessKeyCustomToken', token);
}

export function removeToken() {
    localStorage.removeItem('accessKeyCustomToken');
}
