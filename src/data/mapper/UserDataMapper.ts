import { Auth, InvalidAuth, Error } from '@/types/models.d';
import { checkApiDataValid } from '../utils/checkApiDataValid';

export class UserDataMapper {
  public static mapToAuth(data: any) {
    const auth: Auth = {
      isAuthenticated: true,
      status: 'Login Successfully',
      token: checkApiDataValid(data.token),
    };
    return auth;
  }

  public static mapToInvalidAuth(data: any) {
    const invalidAuth: InvalidAuth = {
      hasError: true,
      errors: [],
    };
    if (data.errors) {
      data.errors.forEach((error: any) => {
        const errors: Error = this.mapToError(error);
        invalidAuth.errors.push(errors);
      });
    }
    return invalidAuth;
  }

  public static mapToError(data: any): Error {
    const errors: Error = {
      field: data.field,
      message: data.message,
    };
    return errors;
  }
}
