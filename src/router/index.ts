import { AuthModule } from './../store/modules/auth';
import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
// import Home from '../views/Home.vue';
import store from '@/store';


Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'Login',
    component: () => import('@/views/Login.vue'),
  },
  {
    path: '/home',
    name: 'Home',
    meta: {
      user: true,
    },
    component: () => import('@/views/Home.vue'),
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  // },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  console.log(to);

  if (to.matched.some((record) => record.meta.user)) {
    if (!AuthModule.isAuthenticated) {
      next('/');
    } else {
      next();
    }
  } else {
    if (AuthModule.isAuthenticated) {
      console.log('to.path');
      console.log(to.path);
      if (to.path === '/') {
        console.log('entered login');
        next(store.getters.routeState);
      } else {
        next();
      }
    } else {
      next();
    }
  }
});

export default router;
