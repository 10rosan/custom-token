import { AuthState } from './modules/auth';
import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';


Vue.use(Vuex);

export interface RootState {
  auth: AuthState;
}

const vuexLocal = new VuexPersistence<RootState>({
  storage: window.localStorage,
});

export default new Vuex.Store<RootState>({
  mutations: {
    RESTORE_MUTATION: vuexLocal.RESTORE_MUTATION,
  },
  modules: {
  },
  plugins: [
    vuexLocal.plugin,
  ],
});
