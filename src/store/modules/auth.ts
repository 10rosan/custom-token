import { UserApi } from './../../data/source/remote/UserApi';
import { UserCredentials, Error } from './../../types/models.d';
import { Action, getModule, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import store from '@/store';
import { getToken, removeToken, setToken } from '@/data/utils/preserved-storage';

export interface AuthState {
    isAuthenticated: boolean;
}

export interface ErrorState {
    error: Error[];
}

@Module({ dynamic: true, store, name: 'auth' })
export default class AuthModulex extends VuexModule implements AuthState {
    public isLoading = false;
    public isAuthenticated = !!getToken();
    public msgStatus = '';
    public token = getToken();
    public hasError = false;
    public errors: Error[] = [];

    get errorList(): Error[] {
        return this.errors;
    }

    @Mutation
    public setLoadingState(state: boolean) {
        this.isLoading = state;
    }

    @Mutation
    public setTokenData(data: string) {
        this.token = data;
        setToken(data);
    }


    @Action
    public async login(user: UserCredentials) {
        // this.context.commit("setLoadingState", true);
        const authApi: UserApi = new UserApi();
        await authApi.loginUser(user)
                .then((response) => {
                    console.log('response');
                    console.log(response);
                    console.log('response');
                    if ( response.token !== '') {
                        this.context.commit('setLoadingState', false);
                        this.context.commit('setTokenData', response.token);
                        this.context.commit('SET_AUTHENTICATION', true);
                        this.context.commit('SET_AUTHENTICATION_MESSAGE', 'Login Successful');

                    } else {
                        this.context.commit('SET_AUTHENTICATION', false);
                        this.context.commit('SET_AUTHENTICATION_MESSAGE', response.status);
                    }
                    return response;

            })
            .catch((e) => {
                console.log('error occured await login');
                this.context.commit('SET_ERROR_DATA', true);
                console.log(e.response);
                console.log(e.response.data);
                console.log(e.response.data.errors);
                console.log(e.response.data.errors[0]);
                console.log('error occured await login');
                this.context.commit('SET_ERROR', e.response.data.errors[0]);
                console.log("****");
                console.log(e.response.data);
                console.log(e.response.data.errors);
                console.log(e.response.data.errors[0]);
                console.log('error occured await login');
                const err = 'has error';
                throw err;
            });
            
    }

    @Action
    public logout() {
        this.context.commit('SET_AUTHENTICATION', false);
        removeToken();
    }

    @Mutation
    private SET_AUTHENTICATION_MESSAGE(msgStatus: string) {
        this.msgStatus = msgStatus;
    }

    @Mutation
    private SET_AUTHENTICATION(isAuthenticated: boolean) {
        this.isAuthenticated = isAuthenticated;
    }

    @Mutation
    private SET_ERROR_DATA(hasError: boolean) {
        this.hasError = hasError;
    }

    @Mutation
    private SET_ERROR(error: Error) {
        this.errors.push(error);
    }
}

export const AuthModule = getModule(AuthModulex);
