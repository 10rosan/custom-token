export interface UserCredentials {
    email: string;
    password: string;
}

export interface Auth {
    isAuthenticated: boolean;
    status: string;
    token: string;
}

export interface InvalidAuth {
    hasError: boolean;
    errors: Error[];
}

export interface Error {
    field: string;
    message: string;
}
